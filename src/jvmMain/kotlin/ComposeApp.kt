import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import dao.DAOSongs
import dao.DatabaseFactory

fun main() = application {
    Window(onCloseRequest = ::exitApplication, title = "Playlist Creator") {
        Surface(modifier = Modifier.fillMaxSize()) {
            val dao: DAOSongs = DatabaseFactory.songDatabase
            SongScaffold(dao)

        }
    }

}



package ui

import androidx.compose.runtime.Composable
import model.Song

@Composable
fun SongEditor(
    song: Song,
    onSave: (String, String, String) -> Unit,
    onCancel: () -> Unit
) {
    SongForm(
        initialTitle = song.name,
        initialArtist = song.artist,
        initialAlbum = song.album,
        onSave = { title, artist, album ->
            onSave(title, artist, album)
        },
        onCancel = onCancel
    )
}
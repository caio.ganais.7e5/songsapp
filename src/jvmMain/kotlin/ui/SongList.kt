package ui

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import model.Song

@Composable
fun SongList(
    songs: List<Song>,
    onEdit: (Song) -> Unit,
    onDelete: (Song) -> Unit
) {
    Text(
        text = "Song List",
        style = MaterialTheme.typography.h5
    )
    Card(elevation = 4.dp) {
        if (songs.isEmpty()) {
            Text(text = "No songs found.")
        } else {
            LazyColumn {
            items(songs.size) { index ->
                val song = songs[index]
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Column {
                        Text(text = song.name, style = MaterialTheme.typography.subtitle1)
                        Text(text = "${song.artist} - ${song.album}", style = MaterialTheme.typography.caption)
                    }

                    Row {
                        IconButton(onClick = { onEdit(song) }) {
                            Icon(Icons.Default.Edit, contentDescription = "Edit")
                        }

                        IconButton(onClick = { onDelete(song) }) {
                            Icon(Icons.Default.Delete, contentDescription = "Delete")
                        }
                    }
                }
            }
        }
        }
    }
}
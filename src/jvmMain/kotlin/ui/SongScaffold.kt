import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dao.DAOSongs
import model.Song
import ui.SongEditor
import ui.SongForm
import ui.SongList

@Composable
fun SongScaffold(dao: DAOSongs) {
    var songs by remember { mutableStateOf(dao.allSongs()) }
    var editingSong by remember { mutableStateOf<Song?>(null) }

    Column(modifier = Modifier.fillMaxSize()) {
        SongForm(
            onSave = { title, artist, album ->
                if (editingSong != null) {
                    dao.editSong(editingSong!!.id, title, artist, album)
                    editingSong = null
                } else {
                    dao.addSong(title, artist, album)
                }
                songs = dao.allSongs()
            },
            onCancel = { editingSong = null }
        )

        Spacer(modifier = Modifier.height(16.dp))

        SongList(
            songs = songs,
            onEdit = { song -> editingSong = song },
            onDelete = { song ->
                dao.deleteSong(song.id)
                songs = dao.allSongs()
            }
        )

        editingSong?.let { song ->
            SongEditor(
                song = song,
                onSave = { title, artist, album ->
                    dao.editSong(song.id, title, artist, album)
                    songs = dao.allSongs()
                    editingSong = null
                },
                onCancel = { editingSong = null }
            )
        }
    }
}

package dao

import org.jetbrains.exposed.sql.Table

object SongTable: Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 255)
    val artist = varchar("artist", 255)
    val album = varchar("album", 255)

    override val primaryKey = PrimaryKey(id)
}
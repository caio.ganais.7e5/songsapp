package dao

import kotlinx.coroutines.Dispatchers
import model.Song
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

class DAOSongsImpl: DAOSongs {

    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db"
    val database = Database.connect(jdbcURL, driverClassName)

    init {
        transaction(database) {
            SchemaUtils.create(SongTable)

        }
        addSong("The Pretender", "Foo Fighters", "Echoes, Silence, Patience & Grace")
        addSong("Let It Die", "Foo Fighters", "Echoes, Silence, Patience & Grace")
        addSong("Erase/Replace", "Foo Fighters", "Echoes, Silence, Patience & Grace")
        addSong("Long Road to Ruin", "Foo Fighters", "Echoes, Silence, Patience & Grace")

    }

    private fun resultRowToSong(resultRow: ResultRow): Song {
        return Song(
            id = resultRow[SongTable.id],
            name = resultRow[SongTable.name],
            artist = resultRow[SongTable.artist],
            album = resultRow[SongTable.album]
        )
    }

    override fun allSongs(): List<Song> = transaction (database){
        SongTable.selectAll().map(::resultRowToSong)
    }

    override fun addSong(name: String, artist: String, album: String): Song? = transaction(database) {
        val result = SongTable.insert {
            it[SongTable.name] = name
            it[SongTable.artist] = artist
            it[SongTable.album] = album
        }
        result.resultedValues?.get(0)?.let {
            resultRowToSong(it)
        }
    }

    override fun deleteSong(id: Int): Boolean = transaction(database) {
        SongTable.deleteWhere { SongTable.id eq id } > 0
    }


    override fun editSong(id: Int, name: String, artist: String, album: String): Boolean = transaction(database) {
        SongTable.update({ SongTable.id eq id }) {
            it[SongTable.name] = name
            it[SongTable.artist] = artist
        } > 0    }
}
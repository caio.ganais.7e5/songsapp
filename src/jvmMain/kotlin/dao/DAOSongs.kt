package dao

import model.Song

interface DAOSongs {
    fun allSongs(): List<Song>

    fun addSong(name: String, artist: String, album: String): Song?

    fun deleteSong(id: Int): Boolean

    fun editSong(id: Int, name: String, artist: String, album: String): Boolean
}